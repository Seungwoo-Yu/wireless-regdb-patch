 wireless-regdb-patcher
========================
This repository contains the automatic universial patcher
which can be used in any kind of linux for wireless-regdb.
This repository also mirrors the original repository.
(git://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git)

You may need the following steps to run wireless-regdb-patcher

Make sure python 3 is installed:
```
python3 --version
```

Clone this project:
```
git clone https://gitlab.com/Seungwoo-Yu/wireless-regdb-patch.git
```

Install fundamental packages in Centos
```
yum install python-devel python-pip
python3 -m pip install future M2Crypto
```

Install fundamental packages in Ubuntu
```
apt install python-dev python-pip
python3 -m pip install future M2Crypto
```

Make the script executable
```
chmod +x wireless-regdb-patch.sh
```

Run the script (root is required)
```
sudo ./wireless-regdb-patch.sh <rootfs mountpath> (-no-make)
```

You can get instructions for the script
```
./wireless-regdb-patch.sh -help
```

Seungwoo "Ryan" Yu
22 July 2019


 wireless-regdb
================
<https://wireless.wiki.kernel.org/en/developers/regulatory/wireless-regdb>

This repository contains the plain text version of the regulatory
database file I maintain for use with Central Regulatory Database
Agent daemon.  Also included is the compiled binary version of this
file signed with my RSA key.  This represents a good faith attempt
to capture regulatory information that is correct at the time of its last
modification.  This information is provided to you with no warranty
either expressed or implied.

Also included are the tools used to compile and sign the regulatory.bin
file as well as a MoinMoin macro used for viewing the database.


 TECHNICAL INFORMATION
=======================

The regulatory information in 'db.txt' is stored in a human-readable
format which can be read using the 'dbparse.py' python module. This
python module is used by the web viewer (Regulatory.py) which is
implemented as a MoinMoin macro (and used on http://wireless.kernel.org)
to allow viewing the database for verification.

The dbparse module is also used by db2bin.py and db2fw.py, the 'compilers'
that compile the database to its binary formats.

For more information, please see the CRDA page:

	https://wireless.wiki.kernel.org/en/developers/regulatory/crda

and the full description of the Linux regulatory stack:

	https://wireless.wiki.kernel.org/en/developers/regulatory

John W. Linville
17 November 2008
