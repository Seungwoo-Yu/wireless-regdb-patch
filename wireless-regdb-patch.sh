#!/bin/bash
# © 2019 Seungwoo "Ryan" Yu - All rights reserved

files=("regulatory.db lib/firmware"
       "regulatory.bin lib/crda"
       "regulatory.db.p7s lib/firmware"
       "regulatory.db.5.gz usr/share/man/man5"
       "regulatory.bin.5.gz usr/share/man/man5")

if [ $(id -u) = 0 ]; then
    if [[ ! -z "$1" && ! $1 =~ ^-.*$ ]]; then
        if [[ "$3" != "-no-make" ]]; then
            echo "[INFO] make"
            make
        else
            echo "[WARN] Skipping make..."
        fi

        echo "[INFO] Compressing man 5 files"
        cp regulatory.bin.5 regulatory.bin.5-copy
        cp regulatory.db.5 regulatory.db.5-copy
        gzip regulatory.bin.5
        gzip regulatory.db.5
        mv regulatory.bin.5-copy regulatory.bin.5
        mv regulatory.db.5-copy regulatory.db.5

        if [[ -f "regulatory.bin" && -f "regulatory.bin.5.gz" && -f "regulatory.db.5.gz" && -f "regulatory.db" && -f "regulatory.db.p7s" ]]; then
            if [ -d "$1" ]; then
                for file_object in "${files[@]}"; do
                    file_directory=`cut -d ' ' -f 2 <<< ${file_object}`
                    if [ -d "$1/$file_directory" ]; then
                        file_name=`cut -d ' ' -f 1 <<< ${file_object}`
                        cp $file_name "$1/$file_directory/$file_name"
                        echo "[INFO] '$file_name' is copied to '$1/$file_directory' successfully."
                    else
                        echo "[ERROR] '$1/$file_directory' is not a folder."
                    fi
                done
            else
                echo "[ERROR] '$1' is not a folder."
            fi
        else
            echo "[ERROR] Failed to retrieve files."
        fi
    else
        if [[ -z "$1" || $1 != "-help" ]]; then
            echo "[ERROR] Incorrect argument. Use -help to show usage."
        else
            echo "[INFO] wireless-regdb-patch <target> (-no-make)"
            echo "       target: A target directory of linux rootfs"
            echo "       -no-make: Skips Make command"
        fi
    fi
else
    echo "[ERROR] Root is required."
fi
